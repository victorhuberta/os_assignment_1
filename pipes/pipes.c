/**
 * Date: 12th May 2016
 * Module: pipes; @Author: Victor Huberta
 * `````````````````````````````````````````````
 * This modules takes care of everything
 * pipes-related.
 *
 * Functionality includes:
 * create a pipe,
 * map a file descriptor to STDIN of a process,
 * map a file descriptor to STDOUT of a process,
 * and open the LINK_PIPE.
 * `````````````````````````````````````````````
 *
 */

#include "../main/main.h"
#include "pipes.h"

void create_pipe(int pipefd[2]) {
    if (pipe(pipefd) == -1) {
        perror("pipe");
        exit(EXIT_FAILURE);
    }
}

void map_to_stdin(int readfd) {
    if (dup2(readfd, STDIN_FILENO) == -1) {
        perror("dup2");
        exit(EXIT_FAILURE);
    }
    close(readfd);
}

void map_to_stdout(int writefd) {
    if (dup2(writefd, STDOUT_FILENO) == -1) {
        perror("dup2");
        exit(EXIT_FAILURE);
    }
    close(writefd);
}

/**
 * Function: open_link_pipe
 *
 * args:
 * mode -> either LINK_READ or LINK_WRITE.
 * ```````````````````````````````````````````
 * Open a named pipe called "link_pipe" in
 * the current directory to link each branch
 * to another.
 * ```````````````````````````````````````````
 *
 */
int open_link_pipe(int mode) {
    int linkfd = open(LINK_PIPE_NAME, mode);

    if (linkfd == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    } else {
        return linkfd;
    }
}
