3	Avoid the Gates of Hell. Use Linux.
3	We all know Linux is great... it does infinite loops in 5 seconds.
0	If Bill Gates is the Devil then Linus Torvalds must be the Messiah.
1	How should I know if it works? That's what beta testers are for. I only coded it.
6	Besides, I think Slackware sounds better than 'Microsoft,' don't you?
4	Those who don't understand Linux are doomed to reinvent it, poorly.
2	Linux _is_ user-friendly. It is not ignorant-friendly and idiot-friendly.
1	Linux hackers are funny people: They count the time in patch levels.
