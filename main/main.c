/**
 * Date: 14th May 2016
 * Module: main; @Author: Victor Huberta
 * ````````````````````````````````````````````````````
 * This module creates a cycle of message passing
 * between N number of processes. The cycle is started
 * by one root process. The cycle consists of two
 * branches of processes i.e. Branch A & Branch B.
 *
 * Every branch has P number of processes and their
 * STDIN & STDOUT are linked by using unnamed pipes.
 * Every process is identified by its index. For a
 * cycle with 4 processes, the index ranges from
 * 0 to 3 (root: 0).
 *
 * Each branch is ended by a process and is linked
 * to another by using a named pipe called the
 * LINK_PIPE. The LINK_PIPE has two ends, LINK_READ
 * and LINK_WRITE. In this program, LINK_WRITE is
 * used by Branch A and LINK_READ is used by Branch
 * B.
 * ````````````````````````````````````````````````````
 * Illustration:
 *
 *    ___Branch B___
 *   |              |
 * ROOT            |P| <-- LINK_PIPE
 *   |___Branch A___|
 *
 */

#include "main.h"
#include "../pipes/pipes.h"
#include "../messages/messages.h"
#include "../branches/branches.h"

int main() {
    int i;

    if (mkfifo(LINK_PIPE_NAME, PIPE_PERMISSION) == -1) {
        perror("mkfifo");
        _exit(EXIT_FAILURE);
    }

    if (atexit(delete_link_pipe) != 0) {
        perror("atexit");
        exit(EXIT_FAILURE);
    }

    /* branch A: index starts from 0 */
    branch(0, LINK_WRITE);

    /* branch B: index starts from length of branch A */
    branch(BRANCH_A_LEN, LINK_READ);

    read_file_and_handle_messages();
    wait_and_handle_messages(0);

    /* wait for 2 immediate child processes */
    for (i = 0; i < 2; ++i) {
        if (wait(NULL) == -1) {
            perror("wait");
            exit(EXIT_FAILURE);
        }
    }

    exit(EXIT_SUCCESS);
}

void delete_link_pipe() {
    if (unlink(LINK_PIPE_NAME) == -1) {
        perror("unlink");
        _exit(EXIT_FAILURE);
    }
}
