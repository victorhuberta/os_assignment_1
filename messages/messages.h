/**
 * Date: 14th May 2016
 * Header: messages; @Author: Victor Huberta
 * ``````````````````````````````````````````
 * Define messages and logs formats, plus
 * all other constants needed for message
 * passing. Several helper macros are
 * defined here as well.
 * ``````````````````````````````````````````
 *
 */

#ifndef MESSAGES_H
#define MESSAGES_H

#define FILE_NAME "messages.txt"
#define END_MSG "END\n"
#define MAX_MSG_SIZE 130

#define TIMESTR_LEN 36
#define LOG_FORMAT "%s\t%s\t%s\n"
#define LOG_NAME_FORMAT "process%d.log"

#define KEEP 0
#define FORWARD 1
#define DISCARD 2

#define IS_PARENT(index) index == 0
#define SEND_END_MSG() write(STDOUT_FILENO, END_MSG, strlen(END_MSG))
#define THIS_IS_THE_END(msg) strncmp(msg, END_MSG, strlen(END_MSG)) == 0
#define MSG_IS_FOR_CURRENT_PROCESS(proc_no, index) proc_no == index

/**
 * Log action to STDERR because STDOUT is used for IPC.
 */
#define LOG_ACTION_TO_SCREEN(text) \
    fprintf(stderr, "process %d: %s\n", getpid(), text)

/**
 * Trim new line from text by spanning it until '\n' is reached.
 */
#define TRIM_NEWLINE(text) text[strcspn(text, "\n")] = 0

/**
 * Read messages from file and pass them on.
 */
void read_file_and_handle_messages();

/**
 * Wait for messages from STDIN and handle them.
 */
void wait_and_handle_messages(int index);

/**
 * Either keep the messages or forward them.
 */
void handle_message(int index, char msg[]);

/**
 * Log all events into a file.
 */
void log_traffic(int index, char msg[], int action);

/**
 * Get timestamp string by combining date time with nanoseconds.
 */
void get_timestr(char timestr[]);
long get_timestamp_nanos();

/**
 * Prepare all needed variables for logging.
 */
void prepare_for_logging(char action_str[], char log_file_name[],
    int index, char msg[], int action);

void log_to_file(char timestr[], char action_str[],
    char log_file_name[], char msg[]);

#endif
